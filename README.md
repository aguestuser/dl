# Overview

`dl` is a program that lets you download big files fast(er) by parallelizing downloads across many threads and cores.

**Table Of Contents**
* [Getting Started](#getting-started)
  * [Sysem Depencencies](#dependencies)
  * [Building dl](#build)
  * [Running dl](#run)
  * [Developing](#develop)
* [Application Design](#design)
* [TODO](#todo)

# Getting Started <a name="geting-started"></a>

## Get the code!

Assuming you don't have the repo yet, get it with:

``` shell
git clone https://0xacab.org/aguestuser/dl.git
cd dl
```

## System Dependencies <a name="dependencies"></a>

You will need `rust` and its build tool, `cargo`, installed to run `dl`. The fastest/simplest way to install them is by running this in a shell:

``` shell
curl https://sh.rustup.rs -sSf | sh
source $HOME/.cargo/env
```

If `curl`-ing bash scripts from the internet into your shell makes you nervous, you can try one of the options [here](https://forge.rust-lang.org/other-installation-methods.html#other-ways-to-install-rustup).

Whatever method you chose, you can check to make sure everything worked with:

``` shell
rustc --version
cargo --version
```

If you would like to update all your packages (likely unnecessary), you can run:

``` shell
rustup update
```

## Building dl <a name="build"></a>

Yay! Now that you have `rust` now, you can build `dl` with:

``` shell
cd <path/to/this/repo>
cargo build --release
```

To make it easier to run, let's put the build artifact on our PATH:

``` shell
sudo ln -s ${PWD}/target/release/dl /usr/bin/dl
```

(If you don't want to do that, that's okay! We can use a slightly longer command later!)

## Running dl <a name="run"></a>

Okay! Time for the main event! Now you can run dl with:

``` shell
dl <url_to_download_from> <path_to_save_file_to>
```

(Filling in <url_to_download_from> and <path_to_save_file_to> with your own values.)

For example, to download this ~20mb copy of `Designing Data-Intensive Applications` (disguised as my resume on RC's S3 bucket), you could run:

``` shell
dl https://recurse-uploads-production.s3.amazonaws.com/dc12e4d0-3c82-45b8-9cb7-6c64a8f50cfb/austin_guest_resume.pdf data/book.pdf
```
If you didn't symlink the build artifact above, you could run:

``` shell
./target/release/dl <url> <path>
```

## Developing dl <a name="develop"></a>

In the above we used production builds because they are faster, and this is a **challenge!** However, if we wanted to hack on the project to change it, we'd want faster build/run cycle than come with the release flag and invoking a binary.

Thankfully, we can build and run in one step with a simple:

``` shell
cargo run
```

We can build and run the tests (with frequency, right?) with:

``` shell
cargo test
```

If you want to check out the (nifty, autogenerated!) docs, you can always run:

``` shell
cargo doc --open
```

And if you want to just re-compile, you can run:

``` shell
cargo build
```


# Application Design <a name="design"></a>

If you want to find your way around the code, starting in `main`, then looking at `lib` and to the modules that it calls is a good way in.

The main action happens in `lib::run`, which is structured as a pipeline of futures, each of which consumes and produces a struct that encapsulates the current state of the program.

More specifically, program flow is modeled a transition between the following structs:

``` shell
Config -> MetadataDownloader -> FileDownloader -> HashChecker
```

This encoding of application state as transitions between structs is a rust convention which, in addition to trying to provide clarity as to what phase of execution the app is in, does some memory management work for us under the hood.

As should be evident from the above, the flow of the program is to:

- accept configs from the user
- make a `HEAD` request to the user-supplied url to:
  - see if it supports range requests, and if so:
  - retrieve the length of the file and (if it exists) its etag
- download the file in several parallel chunks
- take the hash of the downloaded file to see if it matches the advertised etag

Most of the heavy lifting comes in `dl::file::FileDownloader::fetch`. This function:

- creates a blank placeholder file into which chunks will be written as soon as they come off the wire
- calculates optimally-sized chunks in which to download the file, then generates a stream of chunk-sized offsets to use in range requests)
- consumes the stream of offsets and fires of a stream of parallel range requests for the corresponding chunk
- transforms the stream of responses into a stream of buffered bytes, which it writes to the placeholder file after seeking to the correct offset (note: all writes are performed in parallel)
- collects this stream of futures into a single future that resolves successfully if all requests resovle successfully and with failure if any requests fail (yes: we could be less brittle than that in future iterations!**

**A brief note on types:**

Each call to `download_piece` returns a value that we represent as a `Future<u64, DlError>`. The `u64` represents the offset of the downloaded chunk-- in case we wanted to track the status of each request for retries, which we don't currently do (very brittle, I know!).

The`DlError` is a custom error class that serves as an enumerable set of all possible errors we might see in the program's execution. Constructing this type involves a lot of boilerplate. (See `dl::errors`). BUT... having this type around turns out to do a lot of work. For starters, it's **necessary*** to make the "types line up" for all of our variously-typed futures to return an error component of the same type, which allows us to use (monadic) `map`, `map_err`, and `and_then` combinators to manage control flow.

It also makes error-handling in the main loop a breeze, since we have a strong guarantee that our main loop will only ever produce (future) errors of a certain type and that execution will short circuit as soon as one is encountered. Thus, we can just call our main function and `map_err` over its results to print the correct error at the correct time without worrying too much about it. If we wanted to bucket errors into smaller groupings for a larger calling function, that would also be easy. I found all this business with errors annoying at first, but in the end I liked it!

if you see a `Box<Future>** and wonder what's going on: its main purpose is to make sure the compiler has enough information about what kind of future we're returning when we compose Futures of slightly varying types.

**A note on piece size calculation:**

In my original solution, I used a tiered system of coercing piece sizes into a set of tiered bands -- with larger files getting larger piece sizes -- according to [this scheme](http://wiki.depthstrike.com/index.php/Recommendations#Torrent_Piece_Sizes_when_making_torrents) recommended for calculating file piece size for torrents.

However, I found that at very large file sizes (~200MB and above), I would observe the following bugs:

1. `too many open file` errors from the OS (caused by a flood of request causing too many sockets to remain open at the same time)
2. `connection reset by peer` errors from the server (presumably caused by our unthrottled traffic looking like an attack)

Both introduced halting errors, which was a real bummer!

I needed some way to throttle the number of requests being processed at the same time. I briefly investigated using [futures::stream::buffered](https://docs.rs/futures/0.1.27/futures/stream/trait.Stream.html#method.buffered), which is supposed to throttle stream processing, but proved difficult to implement, and outside the scope of this challenge's timeline to figure out.

Instead, I realized I could effectively "throttle" the number of requests by setting a ceiling on the number of pieces I divided the file into, and then requesting them all in one blast (without throttling).

After some benchmarking, I determined that 32 pieces (with a thread pool of 8) seemed to perform the fastest, so I opted for this as my jury-rigged "throttling" solution. I realize it's sort of hacky, but it gets the job done and prevents the program from crashing when trying to download large files, so I counted it as a win!

I would be interested in investigating other throttling strategies were I to continue to develop the project.

# Profiling <a name="profiling"></a>

My main goal in building this project was to build something that was fast, but not necessarily faulttolerant. (And to learn about async programming in rust!)

On this score, I think I did okay. To make sure this was true, I performed some elementary profiling to compare the performance of my parallel solution with a straight-up `GET` request.

The benchmarks from these profiling experiments live with the repo. I performed two rounds of benchmarks (one on my un-throttled solution, and another round on my throttled solution).

Both rounds were both performed on an 8-core Lenovo X1 Carbon with 16GB of RAM running Debian Buster. The first round was performed on a very slow internet connection at my apartment. The second round was performed on a very fast internet connection at Recurse Center (with speeds of 120 Mbps down / 200 Mbps up).

You can view the reports from these benchmarks in the browser with (for example):

``` shell
cd path/to/this/repo
firefox target/criterion/report/index.html
```

## First round

In my first round of benchmarking (performed at home on a slow internet connection**, my goal was to compare my parellel solution to a GET request downloading the same file. I wanted to see if the parallel solution outperformed the sequential version and observe see how this relationship scaled with increasing file sizes.

The upshot was that the parallel solution performs increasingly better than its sequential counterpart the larger the files it is downloading.

- **For small files (on the order of 50KB):** the parallel solution performed roughly the same as a GET request -- downloading files in ~400ms.
- **For medium sized files (on the order of 50MB):** the parallel solution outperformed a GET request by roughly 8x (13s vs 102s)
- **For large files (on the order of 500MB):** I don't have any data available because my solution fell down at that scale.

## Second pass

After settling on a "dumb" method of throttling, I decided to profile, this time with the goals of (1) determine an optimal number of pieces ,and (2) comparing the parallel solution to the sequential solution as above.

For the former trial I compared dividing a file into 8, 16, and 32 pieces (multiples of the number of cores on my machine), I found that 32 pieces (meaning that at most 32 write jobs would be happening concurrently) worked best. At 64, I found that certain servers would get flooded with requests and reset the connection. (Since I had descoped stream buffering, 32 seemed good enough.)

First of all, I noticed that my throttled solution was able to download very large files (up to 2GB) without falling down. Yay!

Secondly, I noticed that with a faster internet connection, it took larger and larger files before I saw any gains from parallelizing. I am not sure exactly how to explain that observation, but it was interesting! More specifically:

- **For small files (~50KB):** The parallel solution was 1.5x slower than a GET request (120ms v 80ms)
- **For medium files (~50MB):** The parallel solution was trivially faster than a GET request (1.1 s v. 1.2 s)
- **For large files (~500MB):** The parallel solution was trivially faster than a GET request (24.3s vs 25.2s)
- **For very large files (~2GB):** The parallel solution was ~3x faster than a GET request (78s vs. 253s)




# TODO <a name="todo"></a>

As noted above, my solution has two major flaws:

1. It does not retry failed chunk requests and suffers halting errors when they happen

Additionally:

2. It relies on discovering the file of the size in advance (to calculate chunk sizes) but has no fallback measures in place for discovering that information in the case that the target server does not supply in in the response to a `HEAD` request.

If I had more time to work on this project, I'd want to solve the above problems in roughly that order. Here are some preliminary thoughts on how I'd do so:

## 1. Handling Failed Requests

To handle failed requests, first we'd want to track the status of all requests. We'd need to store this data in some sort of thread-safe data structure that a bunch of parallel threads could read fro mand write to in parallel without producing contention or draining resources in acquiring locks, etc. My data structure of choice would be some sort of concurrent hash map (which has a series of distribtued mutexes on entries the map, rather than one mutex on the whole map). It looks like rust has a decent implementation in [chashmap](https://docs.rs/chashmap/2.2.2/chashmap/).

When `FileDownloader#fetch` was called, I'd pass it an empty `chashmap`. As requests or writes fail and/or succeed I'd write record that fact to an entry in the `chashmap`. As a first approximation, let's say the keys of the map would be the number of the offset, and the values would be an enum with possible values `Success`, `FailedRequest`, `FailedWrite`. If either a request or a write failed, one of the futures in the stream being collected into one future would fail, meaning that the entire future would fail. I'd `map_err` over this future and recursively call `fetch` again, feeding it the (now not-empty) version of the hash-map.

On subsequent calls, `fetch` should omit offset whose keys contain `Success` values from the stream of offsets it generates for range requests. (Which means that on initial call, it should also consult the empty map to help it construct a full set of offsets.)

To add in further resilience, we could consider the case that execution might terminate in the middle of downloading or writing. To recover from this, we could serialize our `chashmap` on some sane interval and write it to disk, and always make an attempt to build the initial `chashmap` that we pass into `fetch` by deserializing whatever we do (or don't) have stored to disk.

## 2. Fallback file-size discovery

I left a seam for introducing a functional take on a "strategy pattern" solution to this problem in `MetadataDownloader::fetch`. The idea would be that we would call a pipeline of composed functions to try to discover the file size. If any of them find the answer, we return early. If all of them fail, we return an error, causing the caller's execution to short circuit.

I can think of 3 strategies to try (in increasing order of cleverness):

1. Ask for the length in a `HEAD` request (what we currently do). If that fails...
2. Issue a range request for bytes 0-1 of the file. Inspect the `Content-Lenghth` and/or `Content-Range` headers and extract the size of the file from the header values, if present. If that fails...
3. Perform a binary-search like search for the last byte, when you find it, return its position as the file size

As (3) is the only non-trivial solution, its implementation bears some discussion. I'd imagine something like the following:

- write a recursive function that request a one byte chunk of the file at index 2^N, with N starting at 0. if you get data in response increase N by 1 until you don't see a response.
- remeber the first `N` at which you didn't get a response and the last `N` at which you did. call the former `upper` and the latter `lower`
- write a recursive function that implements binary search between `upper` and `lower`:
  - query the server for data at an index halfway between `upper` and `lower`
  - if you find data at an index, set `lower` to that index and recurse
  - if you don't find data at an index, set `upper` to that index and recurse
  - terminate the recursion when `lower` is one less than `upper`, and return `lower`

This should find the size of the file in roughly `O(log N)`, where `N` is the size of the file in bytes.
