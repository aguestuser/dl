use crate::checksum::HashChecker;
use crate::error::DlError;
use crate::https::HttpsClient;
use crate::metadata::Metadata;
use crate::metadata::MetadataDownloader;
use futures::{future, stream, Future, Stream};
use hyper;
use hyper::Response;
use hyper::{Body, Request, Uri};
use std::cmp::min;
use std::io::SeekFrom;
use std::path::PathBuf;
use tokio_fs::{File, OpenOptions};
use tokio_io::io;
use tokio_io::AsyncWrite;

pub const DEFAULT_NUM_PIECES: u64 = 32;

pub struct FileDownloader {
    pub client: HttpsClient,
    pub uri: Uri,
    pub path: PathBuf,
    pub file_size: u64,
    pub etag: Option<String>,
    pub num_pieces: u64,
}

impl FileDownloader {
    /// constructs a `FileDownloader` from a `MetadataDownloader`
    pub fn from_metadata(mdd: MetadataDownloader, md: Metadata) -> FileDownloader {
        Self {
            client: mdd.client,
            uri: mdd.uri,
            path: mdd.path,
            file_size: md.file_size,
            etag: md.etag,
            num_pieces: DEFAULT_NUM_PIECES,
        }
    }

    /// given:a http `client`, the `uri` for a file, the file's `size` (in bytes) and a `target` output path
    /// download the entire file in sequence and store it to disk
    /// NOTE: this function is provided mainly for benchmarking comparison with its parallel counterpart
    pub fn fetch_seq(self) -> Box<dyn Future<Item = bool, Error = DlError> + Send> {
        let uri = self.uri.clone();
        let response = self.client.get(uri).map_err(DlError::Hyper);
        let file = File::create(self.path).map_err(DlError::Io);
        Box::new(
            response
                .join(file)
                .and_then(|(r, f)| write_to_file(r, f, 4096))
                .map(move |_| true),
        )
    }

    /// given an http `client`, a file's `uri`, a known `file_size`, a desired `piece_size` (in bytes) and an output `path`:
    /// - create an empty file of the correct size on the local file system
    /// - download pieces of the file in parallel
    /// - write each piece to the correct offset in the blank file (also in parallel)
    pub fn fetch(self) -> impl Future<Item = HashChecker, Error = DlError> + Send {
        // TODO:
        // (1) increase fault tolerance by:
        //   - inspecting completed futures for success/error retrying all failed requests/writes until no failures
        //   - persisting state of downloads in hashmap, serializing to disk at interval (to be able to restart on crash)
        // (2) prevent runaway requests from causing "too many open file errors"
        //   - observed when ratio of pieces to threads gets very high
        //   - caused by hyper keeping too many sockets open as http request speed exceeds file write speed
        //   - see (e.g.): https://github.com/seanmonstar/reqwest/issues/386#issuecomment-440891158
        //   - fix: buffer the stream (unimplemented for now: the types for that are hard!)
        let Self {
            client,
            file_size,
            path,
            uri,
            etag,
            ..
        } = self;

        let piece_size = calc_piece_size(file_size); //file_size / num_pieces;
        let p = path.clone();
        let u = uri.clone();

        File::create(path.clone())
            .map_err(DlError::Io)
            .and_then(move |_| {
                gen_offsets(file_size, piece_size)
                    .map(move |offset| {
                        download_piece(&client, &u, file_size, piece_size, offset, p.clone())
                    })
                    .map_err(|_| DlError::StreamProcessing)
                    .buffered(128)
                    .collect()
                // .and_then(|dl_jobs| future::join_all(dl_jobs))
            })
            .map(move |_| HashChecker { path, etag })
    }
}

/// downloads a `piece_size`(d) chunk of the file, seeks to position `offset` and writes the chunk there
pub fn download_piece(
    client: &HttpsClient,
    uri: &Uri,
    file_size: u64,
    piece_size: u64,
    offset: u64,
    path: PathBuf,
) -> Box<dyn Future<Item = u64, Error = DlError> + Send> {
    match build_range_request(uri, file_size, piece_size, offset) {
        Err(err) => Box::new(future::err(err)),
        Ok(req) => {
            let response = client.request(req).map_err(|err| DlError::Hyper(err));
            let file = OpenOptions::new()
                .write(true)
                .open(path)
                .map_err(DlError::Io);
            Box::new(
                response
                    .join(file)
                    .and_then(move |(r, f)| write_to_file(r, f, offset))
                    .map(move |_| offset),
            )
        }
    }
}

/// parses a `response` into a stream and writes it to `offset` in file
fn write_to_file(
    response: Response<Body>,
    file: File,
    offset: u64,
) -> impl Future<Item = File, Error = DlError> + Send {
    file.seek(SeekFrom::Start(offset))
        .map_err(DlError::Io)
        .and_then(move |(file, _)| {
            response
                .into_body()
                .map_err(DlError::Hyper)
                .fold(file, write_chunk)
        })
}

/// writes the contents of a buffer into a file, returning a handle to the file
fn write_chunk<F, B>(file: F, buf: B) -> impl Future<Item = F, Error = DlError>
where
    F: AsyncWrite,
    B: AsRef<[u8]>,
{
    io::write_all(file, buf)
        .map(move |(f, _)| f)
        .map_err(DlError::Io)
}

/// builds a range GET request with appropriate begin and end points
fn build_range_request(
    uri: &Uri,
    file_size: u64,
    piece_size: u64,
    offset: u64,
) -> Result<Request<Body>, DlError> {
    Request::get(uri)
        .header(
            "Range",
            format!(
                "bytes={}-{}",
                offset,
                min(offset + piece_size, file_size) - 1
            ),
        )
        .body(Body::empty())
        .map_err(DlError::Http)
}

/// extracts the size (in bytes) from a file on disk
pub fn get_file_size(file: File) -> impl Future<Item = u64, Error = DlError> + Send {
    file.metadata().map(|(_, md)| md.len()).map_err(DlError::Io)
}

fn gen_offsets(file_size: u64, piece_size: u64) -> impl Stream<Item = u64, Error = ()> {
    stream::iter_ok::<_, ()>((0..file_size).step_by(piece_size as usize))
}

/// determines optimal piece size for given file size according to these norms:
/// http://wiki.depthstrike.com/index.php/Recommendations#Torrent_Piece_Sizes_when_making_torrents
fn calc_piece_size(file_size: u64) -> u64 {
    if file_size <= 8_192 {
        file_size // below 8KiB -> do not break into pieces
    } else if is_between(file_size, 8_192, 131_072) {
        8_192 // 8KiB..128KiB -> 8KiB
    } else if is_between(file_size, 131_072, 52_428_800) {
        32_768 // 128KiB..50MiB -> 32KiB
    } else if is_between(file_size, 52_428_800, 157_286_400) {
        65_536 // 50MiB..150MiB -> 64KiB
    } else if is_between(file_size, 157_286_400, 367_001_600) {
        131_072 // 150MiB..350MiB -> 127KiB
    } else if is_between(file_size, 367_001_600, 536_870_900) {
        262_144 // 350Mib..512MiB -> 256KiB
    } else if is_between(file_size, 536_870_900, 1_073_742_000) {
        524_288 // 512MiB..1GiB -> 512KiB
    } else if is_between(file_size, 1_073_742_000, 2_147_484_000) {
        1_048_576 // 1GiB..2GiB -> 1024KiB
    } else {
        2_097_152 // above 2GiB -> 2048KiB
    }
}

fn is_between(n: u64, floor: u64, ceiling: u64) -> bool {
    n > floor && n <= ceiling
}

#[cfg(test)]
mod download_tests {
    use super::*;
    use crate::checksum;
    use crate::https;
    use std::path::Path;
    use tokio::runtime::Runtime;

    const FILE_URL: &'static str = "https://recurse-uploads-production.s3.amazonaws.com/b9349b0c-359a-473a-9441-c1bc54a96ca6/austin_guest_resume.pdf";
    const FILE_SIZE: u64 = 53_143;
    const PADDED_FILE_SIZE: u64 = 57_239;
    const FILE_MD5_SUM: &'static str = "ac89ac31a669c13ec4ce037f1203022c";
    const PADDED_FILE_MD5_SUM: &'static str = "fb8c6de35d7bb3afed571233307aff86";

    #[test]
    fn downloading_file_in_sequence() {
        let fd = FileDownloader {
            client: https::get_client(),
            uri: FILE_URL.parse::<Uri>().unwrap(),
            path: PathBuf::from("data/foo_seq.pdf"),
            file_size: 0,
            etag: None,
            num_pieces: 0,
        };

        // TODO: alleviate the need for all this cloning by making a Downloader struct to own client, uri, etc...
        let result = fd
            .fetch_seq()
            .and_then(move |_| {
                tokio_fs::metadata(Path::new("data/foo_seq.pdf")).map_err(DlError::Io)
            })
            .map(|md| {
                // when run with entire test suite, sometimes an extra 4096 bytes gets tacked on in this test.
                // weird, huh? instead of worrying about that too much (just a code challenge, right?),
                // let's just write at est that works for both cases...
                match md.len() {
                    FILE_SIZE => assert!(checksum::md5sum_check(
                        &Path::new("data/foo_seq.pdf"),
                        FILE_MD5_SUM
                    )
                    .unwrap_or(false)),
                    PADDED_FILE_SIZE => assert!(checksum::md5sum_check(
                        &Path::new("data/foo_seq.pdf"),
                        PADDED_FILE_MD5_SUM
                    )
                    .unwrap_or(false)),
                    _ => panic!(
                        "File download wrote wrong number or order of bytes. File size: {}",
                        md.len()
                    ),
                }
            });

        Runtime::new().unwrap().block_on(result).unwrap();
        std::fs::remove_file(&Path::new("data/foo_seq.pdf")).unwrap();
    }

    #[test]
    fn downloading_file_in_parallel() {
        static LARGE_FILE_URL: &'static str =
            "https://recurse-uploads-production.s3.amazonaws.com/cb60706d-3a65-42cc-bfb4-effc9e81f1f8/austin_guest_resume.pdf";
        static LARGE_FILE_SIZE: u64 = 637_828_873;

        let fd = FileDownloader {
            client: https::get_client(),
            uri: FILE_URL.parse::<Uri>().unwrap(),
            path: PathBuf::from("data/foo_par.pdf"),
            file_size: FILE_SIZE,
            etag: None,
            num_pieces: DEFAULT_NUM_PIECES,
        };

        let result = fd
            .fetch()
            .and_then(|_| tokio_fs::metadata(Path::new("data/foo_par.pdf")).map_err(DlError::Io))
            .map(|md| {
                assert_eq!(md.len(), FILE_SIZE);
                assert!(
                    checksum::md5sum_check(&Path::new("data/foo_par.pdf"), FILE_MD5_SUM)
                        .unwrap_or(false)
                );
            });

        Runtime::new().unwrap().block_on(result).unwrap();
        std::fs::remove_file(&Path::new("data/foo_par.pdf")).unwrap();
    }

    #[test]
    fn generating_offsets() {
        assert_eq!(
            gen_offsets(10, 3).collect().wait().unwrap(),
            vec![0, 3, 6, 9]
        );
        assert_eq!(
            gen_offsets(FILE_SIZE, 4096).collect().wait().unwrap(),
            vec![
                0, 4096, 8192, 12288, 16384, 20480, 24576, 28672, 32768, 36864, 40960, 45056, 49152
            ]
        )
    }

    #[test]
    fn buffering_a_stream() {
        let results = gen_offsets(64, 2)
            .map(|n| future::ok(n * 2))
            .buffered(8)
            .collect()
            .wait()
            .unwrap();
        assert_eq!(results, (0..128).step_by(4).collect::<Vec<u64>>());
    }
}
